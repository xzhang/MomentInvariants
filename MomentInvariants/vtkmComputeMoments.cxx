#include "vtkComputeMoments.h"
#include "vtkMomentsHelper.h"
//#include "vtkMomentsTensor.h"

#if VTK_MODULE_ENABLE_VTK_AcceleratorsVTKm
#include "vtkmlib/ImageDataConverter.h"
#include "vtkmFilterPolicy.h"
#include <vtkm/filter/ComputeMoments.h>
#endif

#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkImageData.h>
#include <vtkPointData.h>

#include <algorithm>
#include <vector>

namespace
{
#if VTK_MODULE_ENABLE_VTK_AcceleratorsVTKm
struct MyPolicy : public vtkm::filter::PolicyBase<MyPolicy>
{
  using FieldTypeList = vtkm::ListTagBase<vtkm::Float32,
                                          vtkm::Float64,
                                          vtkm::Vec<vtkm::Float32, 2>,
                                          vtkm::Vec<vtkm::Float64, 2>,
                                          vtkm::Vec<vtkm::Float32, 3>,
                                          vtkm::Vec<vtkm::Float64, 3>,
                                          vtkm::Vec<vtkm::Float32, 4>,
                                          vtkm::Vec<vtkm::Float64, 4>,
                                          vtkm::Vec<vtkm::Float32, 6>,
                                          vtkm::Vec<vtkm::Float64, 6>,
                                          vtkm::Vec<vtkm::Float32, 9>,
                                          vtkm::Vec<vtkm::Float64, 9>
                                         >;

  using StructuredCellSetList = vtkmInputFilterPolicy::StructuredCellSetList;
  using UnstructuredCellSetList = vtkmInputFilterPolicy::UnstructuredCellSetList;
  using AllCellSetList = vtkmInputFilterPolicy::AllCellSetList;
};

struct ExtractComponentImpl
{
  template <typename T, typename S>
  void operator()(const vtkm::cont::ArrayHandle<T, S>& field,
                  const int *idx,
                  vtkDoubleArray* out) const
  {
    auto portal = field.GetPortalConstControl();
    auto numComps = vtkm::VecTraits<T>::GetNumberOfComponents(portal.Get(0));

    vtkm::IdComponent compIdx = 0;
    switch (numComps)
    {
      case 1:
        compIdx = 0;
        break;
      case 2: case 3:
        compIdx = static_cast<vtkm::IdComponent>(*idx);
        break;
      case 4:
        compIdx = static_cast<vtkm::IdComponent>(idx[1] * 2 + idx[0]);
        break;
      case 6: case 9:
        compIdx = static_cast<vtkm::IdComponent>(idx[0] * 3 + idx[1]);
        break;
      default:
        std::cout << "execution shouldn't reach here\n";
        abort();
    }

    for (vtkm::Id i = 0; i < portal.GetNumberOfValues(); ++i)
    {
      out->SetValue(i, static_cast<double>(vtkm::VecTraits<T>::GetComponent(portal.Get(i), compIdx)));
    }
  }
};

void ExtractComponent(const vtkm::cont::Field& field, const int* idx, vtkDataArray* out)
{
  vtkm::cont::CastAndCall(vtkm::filter::ApplyPolicyFieldNotActive(field, MyPolicy{}),
                          ExtractComponentImpl{},
                          idx,
                          vtkDoubleArray::SafeDownCast(out));
}
#endif
} // anonymous namespace

void vtkComputeMoments::ComputeVtkm(
  int radiusIndex, vtkImageData* grid, vtkImageData* field, vtkImageData* output)
{
  std::cout << "vtkComputeMoments::ComputeVtkm \n";

#if VTK_MODULE_ENABLE_VTK_AcceleratorsVTKm
  int gridDims[3];
  grid->GetDimensions(gridDims);
  const bool is2D = gridDims[2] == 1;

  double fieldSpacing[3];
  field->GetSpacing(fieldSpacing);
//  vtkm::Vec<double, 3> spacing{fieldSpacing};
  vtkm::Vec<double, 3> spacing{fieldSpacing[0], fieldSpacing[1], fieldSpacing[2]};

  if (grid != field)
  {
    int fieldDims[3];
    double gridSpacing[3];
    grid->GetSpacing(gridSpacing);
    field->GetDimensions(fieldDims);

    for (int i = 0; i < 3; ++i)
    {
      if (gridDims[i] != fieldDims[i] || gridSpacing[i] != fieldSpacing[i])
      {
        vtkErrorMacro(<< "The structure of grid and field must be the same for VTK-m");
        return;
      }
    }
  }

  auto fieldArray = field->GetPointData()->GetArray(this->NameOfPointData.c_str());

  try
  {
    // convert the input dataset to a vtkm::cont::DataSet
    vtkm::cont::DataSet in = tovtkm::Convert(field);
    auto field = tovtkm::Convert(fieldArray, vtkDataObject::FIELD_ASSOCIATION_POINTS);
    in.AddField(field);
    //in.PrintSummary(std::cout);

    vtkm::filter::ComputeMoments computeMoments;
    computeMoments.SetOrder(this->Order);
    computeMoments.SetSpacing(spacing);
    computeMoments.SetRadius(this->Radii.at(radiusIndex));
    computeMoments.SetActiveField(this->NameOfPointData);

    vtkm::cont::DataSet out = computeMoments.Execute(in, MyPolicy{});
    //out.PrintSummary(std::cout);

    std::vector<int> indices;
    for (int order = 0; order <= this->Order; ++order)
    {
      const int maxR = is2D ? 0 : order; // 2D grids don't use r
      for (int r = 0; r <= maxR; ++r)
      {
        const int qMax = order - r;
        for (int q = 0; q <= qMax; ++q)
        {
          const int p = order - r - q;

          indices.resize(order);

          // Fill indices according to pqr values:
          if (!indices.empty())
          {
            auto iter = indices.begin();
            iter = std::fill_n(iter, p, 0);
            iter = std::fill_n(iter, q, 1);
            iter = std::fill_n(iter, r, 2);
            assert(iter == indices.end());
          }

          auto vtkmFieldName = std::string("index");
          for (int i : indices)
          {
            vtkmFieldName += std::to_string(i);
          }

//          std::cerr << "Order: " << order << " "
//                    << "pqr: " << p << "x" << q << "x" << r << " "
//                    << "Field name: " << vtkmFieldName << "\n";

          auto field = out.GetField(vtkmFieldName);

          auto numComps = static_cast<int>(std::pow(this->Dimension, this->FieldRank));
          for (int c = 0; c < numComps; ++c)
          {
            indices.resize(order);
            for (int i = 0; i < this->FieldRank; ++i)
            {
              indices.push_back((c / static_cast<int>(std::pow(this->Dimension, i))) % this->Dimension);
            }

            auto vtkFieldName =
                vtkMomentsHelper::getFieldNameFromTensorIndices(this->Radii.at(radiusIndex),
                                                                indices,
                                                                this->FieldRank);
            ExtractComponent(field,
                             indices.data() + order,
                             output->GetPointData()->GetArray(vtkFieldName.c_str()));
          }
        }
      }
    }
  }
  catch (const vtkm::cont::Error& e)
  {
    vtkErrorMacro(<< "VTK-m error: " << e.GetMessage());
    return;
  }
#else
  vtkErrorMacro(<< "Please enable AcceleratorVTKm to use GPU");
#endif
}
